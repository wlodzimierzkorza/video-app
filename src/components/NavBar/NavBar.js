import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, NavbarBrand, NavbarToggler, Collapse, NavItem, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { createURL } from '../../functions';
import { getVideo } from '../../redux/actions';
import videos from '../../videos.json';

const NavBar = ({ getVideo, providers }) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    const loadVideos = () => {
        Object.keys(videos).forEach((key) => {
            const provider = providers.find((provider) => provider.name === key);
            setTimeout(() => {
                videos[key].forEach((id) => {
                    getVideo(createURL(id, provider), 'videos', provider.name);
                });
            }, 1000);
        });
    };

    return (
        <Navbar light expand="md">
            <NavbarBrand href="/">Video app</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="mr-auto" navbar>
                    <NavItem>
                        <NavLink to="/">Dodaj film</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink to="/videos">Lista filmów</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
            <Button onClick={loadVideos}>Wczytaj video</Button>
        </Navbar>
    );
};

const mapDispatchToProps = (dispatch) => {
    return {
        getVideo: (link, type, provider) => {
            dispatch(getVideo(link, type, provider));
        },
    };
};
const mapStateToProps = ({ providers }) => {
    return { providers };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
