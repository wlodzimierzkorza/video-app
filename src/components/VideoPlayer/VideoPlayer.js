import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

const VideoPlayer = ({ video, providers }) => {
    const [url, setURL] = useState('');
    useEffect(() => {
        if (video) {
            const url = `${
                providers.find((provider) => {
                    return provider.name === video.provider;
                }).playerURL
            }${video.providerId}`;
            setURL(url || '');
        }
    }, [video]);

    return (
        <>
            {video && (
                <iframe
                    title={video.title}
                    id={video.providerId}
                    type="text/html"
                    width="100%"
                    height="400"
                    src={url}
                    frameBorder="0"
                    allow="autoplay; fullscreen; picture-in-picture"
                    allowFullScreen
                />
            )}
        </>
    );
};

const mapStateToProps = ({ providers }) => {
    return { providers };
};

export default connect(mapStateToProps, null)(VideoPlayer);
