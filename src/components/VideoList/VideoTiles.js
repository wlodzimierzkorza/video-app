import React from 'react';
import { Row, Col } from 'reactstrap';
import VideoListItem from './VideoItem';

const VideoTiles = ({ videos }) => {
    return (
        <Row>
            {videos &&
                videos.map((video) => {
                    return (
                        <Col md={4} xs={12} sm={6} lg={4} key={video.id}>
                            <VideoListItem customClass="tiles" video={video} />
                        </Col>
                    );
                })}
        </Row>
    );
};

export default VideoTiles;
