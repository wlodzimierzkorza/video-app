import React from 'react';
import { Row, Col } from 'reactstrap';
import VideoListItem from './VideoItem';

const VideoList = ({ videos }) => {
    return (
        <Row>
            <Col xs={12}>
                {videos &&
                    videos.map((video) => {
                        return <VideoListItem customClass="list" video={video} key={video.id} />;
                    })}
            </Col>
        </Row>
    );
};

export default VideoList;
