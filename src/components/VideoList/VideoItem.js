import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Row, Col, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { updateItem, removeItem } from '../../redux/actions';
import VideoPlayer from '../VideoPlayer/VideoPlayer';

const VideoListItem = ({ customClass, video, updateItem, removeItem }) => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    const { title, thumbnail, likes, date, counts, favorite, id } = video;

    return (
        <>
            <div className={`video-container ${customClass}`}>
                <div className="thumbnail">
                    <button type="button" onClick={toggle}>
                        <img src={thumbnail} alt={title} />
                    </button>
                </div>
                <div className="video-description">
                    <span className="date">Dodano: {date}</span>
                    <h4>{title}</h4>
                    <div className="video-data">
                        <span>Odtworzeń: {counts || ''}</span>
                        <span>Polubiony: {likes} </span>
                    </div>
                    <div className="video-actions">
                        <Row>
                            <Col>
                                {favorite === false && (
                                    <Button
                                        block
                                        color="danger"
                                        outline
                                        size="sm"
                                        onClick={() =>
                                            updateItem('videos', { ...video, favorite: true })
                                        }
                                    >
                                        Ulubione
                                    </Button>
                                )}
                                {favorite && (
                                    <Button
                                        block
                                        color="danger"
                                        outline
                                        size="sm"
                                        onClick={() =>
                                            updateItem('videos', { ...video, favorite: false })
                                        }
                                    >
                                        Usuń z ulubionych
                                    </Button>
                                )}
                            </Col>
                            <Col>
                                <Button
                                    block
                                    color="success"
                                    size="sm"
                                    outline
                                    onClick={() => removeItem('videos', id)}
                                >
                                    Usuń
                                </Button>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            {video && (
                <Modal isOpen={isOpen} toggle={toggle} size="lg">
                    <ModalHeader>{title}</ModalHeader>
                    <ModalBody>
                        <VideoPlayer video={video} />
                    </ModalBody>
                </Modal>
            )}
        </>
    );
};

const mapStateToProps = (state) => {
    return { state };
};
const mapDispatchToProps = (dispatch) => {
    return {
        updateItem: (type, item) => {
            dispatch(updateItem(type, item));
        },
        removeItem: (type, id) => {
            dispatch(removeItem(type, id));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(VideoListItem);
