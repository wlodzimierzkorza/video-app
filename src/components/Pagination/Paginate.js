import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

const Paginate = ({ pagination, setPagination }) => {
    return (
        <div className="pagination-container">
            <Pagination>
                {pagination.pagesNumbers.map((item) => {
                    return (
                        <PaginationItem key={item}>
                            <PaginationLink
                                onClick={() => {
                                    const { begin, end, step } = pagination;
                                    setPagination({
                                        ...pagination,
                                        begin: step * item - step,
                                        end: step * item,
                                    });
                                }}
                            >
                                {item}
                            </PaginationLink>
                        </PaginationItem>
                    );
                })}
            </Pagination>
        </div>
    );
};

export default Paginate;
