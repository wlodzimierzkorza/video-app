import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducer from './reducers';

const { REACT_APP_VIMEO_KEY, REACT_APP_YOUTUBE_KEY } = process.env;

const initialState = {
    videos: [],
    alert: {
        message: '',
        color: '',
        visible: false,
    },
    providers: [
        {
            name: 'youtube',
            api: `https://youtube.googleapis.com/youtube/v3/videos?part=snippet,statistics,player&`,
            params: ['id', 'key'],
            playerURL: 'http://www.youtube.com/embed/',
            enpoint: '',
            domains: ['youtu.be', 'youtube.com'],
            access: {
                slug: 'key',
                key: REACT_APP_YOUTUBE_KEY,
            },
        },
        {
            name: 'vimeo',
            api: 'https://api.vimeo.com/videos',
            playerURL: 'https://player.vimeo.com/video/',
            params: ['access_token'],
            endpoint: 'id',
            domains: ['vimeo.com'],
            access: {
                slug: 'access_token',
                key: REACT_APP_VIMEO_KEY,
            },
        },
    ],
};

const savetoLocalStorage = (state) => {
    const serializedState = JSON.stringify(state);
    localStorage.setItem('video-app', serializedState);
};
const loadFromLocalStorage = () => {
    const serializedState = localStorage.getItem('video-app');

    if (serializedState === null) {
        return initialState;
    }
    return JSON.parse(serializedState);
};

const persistentState = loadFromLocalStorage();

const store = createStore(reducer, persistentState, composeWithDevTools(applyMiddleware(thunk)));

store.subscribe(() => savetoLocalStorage(store.getState()));

export default store;
