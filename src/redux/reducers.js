import { REMOVE_ITEM, ADD_ITEM, UPDATE_ITEM, ADD_OBJECT_ITEM } from './actions';

export default (state, { type, payload }) => {
    switch (type) {
        case ADD_ITEM:
            return {
                ...state,
                [payload.type]: [...state[payload.type], payload.item],
            };
        case ADD_OBJECT_ITEM:
            return {
                ...state,
                [payload.type]: payload.item,
            };
        case UPDATE_ITEM:
            return {
                ...state,
                [payload.type]: [
                    ...state[payload.type].map((item) => {
                        return item.id === payload.data.id ? payload.data : item;
                    }),
                ],
            };
        case REMOVE_ITEM:
            return {
                ...state,
                [payload.type]: [...state[payload.type]].filter((item) => {
                    return item.id !== payload.id;
                }),
            };

        default:
            return state;
    }
};
