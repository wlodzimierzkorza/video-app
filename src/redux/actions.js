import axios from 'axios';
import { createVidoObject } from '../functions';

export const REMOVE_ITEM = 'REMOVE_ITEM';
export const ADD_ITEM = 'ADD_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const REMOVE_IREM = 'REMOVE_ITEM';
export const ADD_OBJECT_ITEM = 'ADD_OBJECT_ITEM';

export const addItem = (type, item) => (dispatch) => {
    return dispatch({
        type: ADD_ITEM,
        payload: {
            type,
            item,
        },
    });
};
export const addObjectItem = (type, item) => (dispatch) => {
    return dispatch({
        type: ADD_OBJECT_ITEM,
        payload: {
            type,
            item,
        },
    });
};

export const getVideo = (url, type, provider) => (dispatch) => {
    return axios
        .get(url)
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type: ADD_ITEM,
                    payload: {
                        type,
                        item: createVidoObject(res.data, provider),
                    },
                });
                dispatch({
                    type: ADD_OBJECT_ITEM,
                    payload: {
                        type: 'alert',
                        item: {
                            color: 'success',
                            message: 'Film został dodany',
                            visible: true,
                        },
                    },
                });
            }
        })
        .catch((err) => {
            dispatch({
                type: ADD_OBJECT_ITEM,
                payload: {
                    type: 'alert',
                    item: {
                        color: 'danger',
                        message: 'FWystapił błąd. Spróbuj ponownie za chwilę',
                        visible: true,
                    },
                },
            });
        });
};

export const updateItem = (type, data) => (dispatch) => {
    return dispatch({
        type: UPDATE_ITEM,
        payload: {
            type,
            data,
        },
    });
};

export const removeItem = (type, id) => (dispatch) => {
    dispatch({
        type: REMOVE_ITEM,
        payload: {
            type,
            id,
        },
    });
};
