import React from 'react';
import { Container } from 'reactstrap';
import NavBar from '../components/NavBar/NavBar';

const DefaultTemplate = ({ children }) => {
    return (
        <>
            <NavBar />
            <Container className="themed-container">{children}</Container>
        </>
    );
};

export default DefaultTemplate;
