import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
    Row,
    Button,
    Col,
    Input,
    Pagination,
    PaginationLink,
    PaginationItem,
    Alert,
} from 'reactstrap';
import VideoList from '../components/VideoList/VideoList';
import VideoTiles from '../components/VideoList/VideoTiles';
import { sortArray } from '../functions';
import Paginate from '../components/Pagination/Paginate';

const VideoListView = ({ videos }) => {
    const [videosArray, setVideosArray] = useState([]);
    const [displayType, setDisplayType] = useState(true);
    const [favorites, setFavorites] = useState(false);
    const [pagination, setPagination] = useState({
        begin: 0,
        end: 9,
        step: 10,
        pagesNumbers: [],
    });

    useEffect(() => {
        setVideosArray(sortArray(videos, 'timestamp', 'asc'));

        if (favorites) {
            const array = [...videosArray];
            setVideosArray(
                array.filter((video) => {
                    return video.favorite === true;
                }),
            );
        }
    }, [videos]);

    useEffect(() => {
        const array = [];
        const pages = Math.ceil(videosArray.length / pagination.step);
        for (let i = 0; i < pages; i++) {
            array.push(i + 1);
        }
        setPagination({
            ...pagination,
            pagesNumbers: array,
        });
    }, [videosArray]);

    const handleOnlyFavorites = () => {
        setVideosArray(
            videosArray.filter((video) => {
                return video.favorite === true;
            }),
        );
        setFavorites(true);
    };
    const handleShowAll = () => {
        setVideosArray(videos);
        setFavorites(false);
    };

    const handleSort = (e) => {
        const array = [...videosArray];
        setVideosArray(sortArray(array, 'timestamp', e.target.value));
    };

    return (
        <>
            <Row>
                <Col md={2} xs={4}>
                    <Button block color="primary" outline onClick={() => setDisplayType(true)}>
                        Lista
                    </Button>
                </Col>
                <Col md={2} xs={4}>
                    <Button block color="primary" outline onClick={() => setDisplayType(false)}>
                        Kafelki
                    </Button>
                </Col>
                <Col md={2} xs={6}>
                    {favorites && (
                        <Button block color="info" outline onClick={handleShowAll}>
                            Wszystkie
                        </Button>
                    )}
                    {favorites === false && (
                        <Button
                            block
                            color="info"
                            outline
                            onClick={() => {
                                handleOnlyFavorites();
                                setPagination({
                                    ...pagination,
                                    begin: 0,
                                    end: 9,
                                });
                            }}
                        >
                            Ulubione
                        </Button>
                    )}
                </Col>
                <Col md={3} xs={4}>
                    <Input type="select" defaultValue="asc" onChange={handleSort}>
                        <option value="asc">Od najnowszego</option>
                        <option value="desc">Od najstarszego</option>
                    </Input>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Paginate pagination={pagination} setPagination={setPagination} />
                </Col>
            </Row>
            {displayType ? (
                <VideoList videos={videosArray.slice(pagination.begin, pagination.end)} />
            ) : (
                <VideoTiles videos={videosArray.slice(pagination.begin, pagination.end)} />
            )}
            <Row>
                <Col>
                    <Paginate pagination={pagination} setPagination={setPagination} />
                </Col>
            </Row>
        </>
    );
};

const mapStateToProps = ({ videos }) => {
    return { videos };
};
export default connect(mapStateToProps, null)(VideoListView);
