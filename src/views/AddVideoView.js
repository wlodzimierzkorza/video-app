import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Input, Row, Col, Button, InputGroup, InputGroupAddon, Alert } from 'reactstrap';
import { createURL } from '../functions';
import { getVideo } from '../redux/actions';

const AddVideoView = ({ providers, getVideo, alert }) => {
    const [val, setVal] = useState('');
    const [alerts, setAlert] = useState(alert);
    const extractID = (link) => {
        return link.split('&').shift().split('/').pop().split('=').pop();
    };

    const getVideoFromUrl = (provider, link) => {
        const id = extractID(link);
        providers.forEach((item) => {
            if (item.name === provider) {
                const add = getVideo(createURL(id, item), 'videos', item.name, id);
                console.log(add);
            }
        });
    };

    const getvideoFromID = (id) => {
        providers.forEach((provider) => {
            getVideo(createURL(id, provider), 'videos', provider.name, id);
        });
        setVal('');
    };

    const providerUrlDetector = (link) => {
        let provider = '';
        providers.forEach((el) => {
            el.domains.forEach((domain) => {
                if (link.indexOf(domain) !== -1) provider = el.name;
            });
        });
        getVideoFromUrl(provider, link);
    };

    const handleSave = () => {
        const urlRegex = /(https?:\/\/[^\s]+)/g;

        if (urlRegex.test(val) === false) {
            getvideoFromID(val);
            return;
        }
        providerUrlDetector(val);
        setVal('');
    };

    useEffect(() => {
        setAlert(alert);
        alert.visible &&
            setTimeout(() => {
                setAlert({
                    message: '',
                    visible: false,
                    color: '',
                });
            }, 3000);
    }, [alert]);

    useEffect(() => {
        setAlert({
            ...alerts,
            visible: false,
        });
    }, []);

    return (
        <>
            <Row form>
                <Col md={{ size: 6, offset: 3 }}>
                    <h3 className="text-center">Dodaj film</h3>
                    <InputGroup>
                        <Input name="link" onChange={(e) => setVal(e.target.value)} value={val} />
                        <InputGroupAddon addonType="prepend">
                            <Button color="primary" onClick={handleSave}>
                                Dodaj
                            </Button>
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Alert className="text-center" color={alerts.color} isOpen={alerts.visible}>
                        {alerts.message}
                    </Alert>
                </Col>
            </Row>
        </>
    );
};

const mapStateToProps = ({ providers, alert }) => {
    return {
        providers,
        alert,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getVideo: (url, type, provider, id) => {
            dispatch(getVideo(url, type, provider, id));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddVideoView);
