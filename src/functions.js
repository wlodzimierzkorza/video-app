export const createURLFromIds = (provider, array = [], id = null) => {
    const obj = {
        id: id === null ? array.join(',') : id,
        links: array,
        api: array.length === 0 ? provider.api : provider.search,
        params: array.length === 0 ? provider.params : provider.searchParams,
        endpoint: provider.endpoint,
        [provider.access.slug]: provider.access.key,
    };

    let params = '';
    const endpoint = obj.endpoint ? `/${obj[obj.endpoint]}?` : '';
    obj.params.forEach((param) => {
        params += `${param}=${obj[param]}&`;
    });

    return `${obj.api}${endpoint}${params}`;
};
export const createURL = (id, provider) => {
    const obj = {
        id,
        api: provider.api,
        params: provider.params,
        endpoint: provider.endpoint,
        [provider.access.slug]: provider.access.key,
    };
    let params = '';
    const endpoint = obj.endpoint ? `/${obj[obj.endpoint]}?` : '';
    obj.params.forEach((param) => {
        params += `${param}=${obj[param]}&`;
    });

    return `${obj.api}${endpoint}${params}`;
};

export const createVidoObject = (data, provider) => {
    const now = Date.now();
    const date = Intl.DateTimeFormat('pl', {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric',
    }).format(now);

    const id = `_${Math.random().toString(36).substring(2, 9)}`;

    switch (provider) {
        case 'youtube':
            return {
                provider,
                timestamp: now,
                providerId: data.items[0].id,
                id,
                date,
                favorite: false,
                title: data.items[0].snippet.title,
                likes: data.items[0].statistics.likeCount,
                thumbnail: data.items[0].snippet.thumbnails.standard.url,
                playerUrl: data.items[0].playerUrl,
                counts: data.items[0].statistics.viewCount,
            };

        case 'vimeo':
            return {
                provider,
                timestamp: now,
                id,
                providerId: data.uri.split('/').pop(),
                date,
                favorite: false,
                title: data.name,
                likes: data.metadata.connections.likes.total,
                thumbnail: data.pictures.sizes[3].link,
                playerUrl: data.playerUrl,
                counts: data.stats.play,
            };

        default:
            return {};
    }
};

export const sortArray = (array, param, order) => {
    return array.sort((a, b) => {
        if (order === 'desc') {
            return a[param] < b[param] ? -1 : 1;
        }
        return a[param] < b[param] ? 1 : -1;
    });
};
