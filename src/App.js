import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import AddVideoView from './views/AddVideoView';
import VideoListView from './views/VideoListView';
import DefaultTemplate from './templates/DefaultTemplate';

const App = () => {
    return (
        <BrowserRouter>
            <DefaultTemplate>
                <Switch>
                    <Route path="/" exact component={AddVideoView} />
                    <Route path="/videos" component={VideoListView} />
                </Switch>
            </DefaultTemplate>
        </BrowserRouter>
    );
};

export default App;
